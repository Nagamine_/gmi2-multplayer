extends Control
@onready var client = $Client
const DEFAULT_PORT = 8910
const address = "ws://godot-hole-puncher.fly.dev"
const peers = []

@onready var pong = preload("res://minigames/pong/pong.tscn").instantiate()
@onready var text: TextEdit = $"ColorRect/TextEdit"
# Called when the node enters the scene tree for the first time.
func _ready():
	# Connect all the callbacks related to networking.
	multiplayer.peer_connected.connect(self._player_connected)
	multiplayer.connection_failed.connect(self._connected_fail)
	multiplayer.connected_to_server.connect(self._connected_ok)
	client.lobby_joined.connect(self._lobby_joined)
	client.lobby_sealed.connect(self._lobby_sealed)
	client.connected.connect(self._connected)
	client.disconnected.connect(self._disconnected)
func _connected(id,a):
	_log("[Signaling] Server connected with ID: %d" % id)


func _disconnected():
	_log("[Signaling] Server disconnected: %d - %s" % [client.code, client.reason])
	$ColorRect/error.text = client.reason


func _lobby_joined(lobby):
	$HostUI/TextEdit.text = lobby
	_log("[Signaling] Joined lobby %s" % lobby)


func _lobby_sealed():
	_log("[Signaling] Lobby has been sealed")


func _log(msg):
	print(msg)
enum playerEnum {PLAYER1,PLAYER2}
var currentplayer
func _on_host_button_pressed():
	print("H")
	# Start as server.
	client.stop()
	currentplayer = playerEnum.PLAYER1
	setPlayer(playerEnum.PLAYER1,multiplayer.get_unique_id(),"")
	text.editable = false
	$ColorRect/HostButton.disabled = true
	$HostUI.visible = true
	$ColorRect.visible = false
	
func setPlayer(player:playerEnum,id,room):
	client.start(address,room, true)
	pass

func _player_connected(id):
	print("connest")
	if(multiplayer.is_server()):
		get_parent().add_child(pong)
	hide()
func _connected_fail():
	print("ERROR")
func _connected_ok():
		print("s")
func _on_join_button_pressed():
	print("J")
	if(text.text != ""):
		text.text = text.text.to_upper()
		setPlayer(playerEnum.PLAYER2,multiplayer.get_unique_id(),text.text)
		currentplayer = playerEnum.PLAYER2
	else:
		$ColorRect/error.text = "Sala no existe"


func _on_text_edit_text_changed():
	var caret = text.get_caret_column()
	text.text = text.text.to_upper()
	text.set_caret_column(caret)
