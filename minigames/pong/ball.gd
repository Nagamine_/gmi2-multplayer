class_name PongBall
extends Area2D

const DEFAULT_SPEED = 400
var direction = Vector2.LEFT

signal score(id)

@export var _speed = DEFAULT_SPEED
@export var synced_direction = Vector2.LEFT:
	set(dir):
		synced_direction = dir
		if(!is_multiplayer_authority()):
			direction = dir
@export var stoped = true
@export var synced_position : Vector2:
	set(val):
		synced_position = val
		if(!is_multiplayer_authority()):
			position = val
@onready var _initial_pos = position
func getRandomDir():
	var n = randf()
	if(n > 0.5):
		n = 1
	else:
		n =-1
	return Vector2(n, 0.5)

func start_game():
	if(is_multiplayer_authority()):
		syncDir.rpc(getRandomDir())
	stoped = false

@rpc("any_peer","call_local", "reliable")
func syncDir(dir):
	direction = dir

func _ready():
	if(!is_multiplayer_authority()):
		syncDir.rpc(getRandomDir())
var last_synced := synced_position
func _process(delta):
	if(is_multiplayer_authority()):
		synced_position = position
		synced_direction = direction
	if(!stoped):
		position += _speed * delta * direction
		#else:
			#position = synced_position
			#direction = synced_direction
			#last_synced = synced_position

@rpc("any_peer","call_local", "reliable")
func reset():
	await get_tree().create_timer(0.3).timeout
	position = _initial_pos
	stoped = true
	if is_multiplayer_authority():
		syncDir.rpc(getRandomDir())
	await get_tree().create_timer(1).timeout
	stoped = false
	position = _initial_pos
	_speed = DEFAULT_SPEED

@rpc("any_peer","call_remote", "reliable")
func sync_position(pos):
	position = pos

@rpc("any_peer","call_local", "reliable")
func bounce(pos,dir):
	direction.x = -direction.x #Vector2(direction.x * -1, randf() * 2 - 1).normalized()
	position = pos
	#sync_position(pos)
	_speed = _speed *1.05
	
#@rpc("authority","call_local", "reliable")
func bounce_vertical(pos,dir):
	direction.y = -direction.y
#

func _on_area_entered(area):
	if(area is PongPlayer and area.playerId == multiplayer.get_unique_id()):
		bounce.rpc(position,direction)
	elif area is PongWall:
		#Sif(is_multiplayer_authority()):
		bounce_vertical(position,direction)
	elif area is PongGoal and area.playerId == multiplayer.get_unique_id():
		score.emit(multiplayer.get_unique_id())
		reset.rpc()
