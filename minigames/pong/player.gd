class_name PongPlayer
extends Area2D

const MOTION_SPEED = 400

@onready var _screen_size_y := 720
@onready var spriteOffset : float = ($Sprite2D.scale.y * $Sprite2D.texture.get_height())/2
@export var playerId := 1 :
	set(id):
		playerId = id
		# Give authority over the player input to the appropriate peer.
		set_multiplayer_authority(id)
		print(name,is_multiplayer_authority(),id)
		set_process(is_multiplayer_authority())
		print("P")
# Called when the node enters the scene tree for the first time.
func _ready():
	# Only process for the local player.
	# set_multiplayer_authority(playerId)
	set_process(is_multiplayer_authority())
	print("a",name,is_multiplayer_authority(),playerId)


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var dir := Input.get_axis(&"up", &"down")
	translate(Vector2(0, dir *MOTION_SPEED * delta))
	position.y = clamp(position.y, spriteOffset, _screen_size_y - spriteOffset)
