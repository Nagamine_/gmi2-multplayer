extends Node

@onready var player1: PongPlayer = $player
@onready var player2: PongPlayer = $player2

func _ready():
	if  multiplayer.is_server():
		player2.playerId = multiplayer.get_peers()[0]
		$Goal2.playerId = player2.playerId
	else:
		# For the client, give control of player 2 to itself.
		player2.playerId = multiplayer.get_unique_id()
		$Goal2.playerId = player2.playerId
		$Control/ColorRect/Button.hide()
		$Control/ColorRect/Label.text = "Espera a que el host inicie la partida" 
		#$ball/MultiplayerSynchronizer.set_multiplayer_authority(1)

@rpc("authority","call_local", "reliable")
func start_game():
	$ball.start_game()
	$Control.hide()

func _on_button_pressed():
	start_game.rpc()

@rpc("any_peer","call_local","reliable")
func add_score(id):
	if($Goal2.playerId == id):
		$ColorRect/PointsLeft.text = str(int($ColorRect/PointsLeft.text)+1)
	if($Goal.playerId == id):
		$ColorRect/PointsRight.text = str(int($ColorRect/PointsRight.text)+1)

func _on_ball_score(id):
	print("ssss",id)
	add_score.rpc(id)
