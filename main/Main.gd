extends Node

var pong = preload("res://minigames/pong/pongMain.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func _on_main_menu_game_start(game):
	$MainMenu.queue_free()
	var pong_instance = pong.instantiate()
	add_child(pong_instance)
